import {
    GET_PRODUCT,
    SET_FAVOURITE,
    ADD_TO_BASKET,
    removeItemBasket,
    REMOVE_ITEM_BASKET,
    CHANGE_MODAL, CLEAR_BASKET
} from "../actions/action";

let initialState = {
    products: [],
    basket: JSON.parse(localStorage.getItem("basket")) || [],
    favorite: JSON.parse(localStorage.getItem("favorite")) || [],
    modal:{type: null, id: null}
}

export const rootReducer = (state = initialState, action) => { // функцияи котороы взаимодействуют с глобальным обьектом store и обновляет его
    switch (action.type) {
        case GET_PRODUCT:
            return {
                ...state,
                products: action.payload
            };
        case SET_FAVOURITE: {
            const {favorite} = state
            const {payload: id} = action
            const nextFavorite = favorite.includes(id)
                ? favorite.filter(productId => productId !== id)
                : [...favorite, id]

            localStorage.setItem("favorite", JSON.stringify(nextFavorite))
            return {
                ...state,
                favorite: nextFavorite
            }
        }
        case ADD_TO_BASKET:{const {basket}=state
            const {payload: id} = action
            if (basket.includes(id)) return state
            const nextBasket = [...basket, id]
            localStorage.setItem("basket", JSON.stringify(nextBasket))
            return {
                ...state,
                basket: nextBasket
            }}
        case REMOVE_ITEM_BASKET :{
            const {basket}=state
            const {payload: id} = action
            const newBasket = basket.filter(element => element !== id)
            localStorage.setItem("basket", JSON.stringify(newBasket))
            return  {
                ...state,
                basket: newBasket
            }
        }
        case CLEAR_BASKET:{
            localStorage.removeItem("basket")
            return  {
                ...state,
                basket:[]
            }
        }
        case CHANGE_MODAL:{
            return  {
                ...state,
                modal:action.payload
            }
        }


    }

    return initialState;
}