import './Modal.module.css';
import Button from "../Button/Button";
import styles from "./Modal.module.css"
import PropTypes from "prop-types";
import Header from "../Header/Header";
function Modal ({header, closeButton, text, actions, click}) {
        return <div className={styles.wrapper} onClick={click}>
            <div role="modalContainer" className={styles.modal} onClick={(e) => {e.stopPropagation()}}>
                <h2>{header}</h2>
                {closeButton && <Button data-testid="closeButton" className={styles.closeBtn} text={"X"}
                                        click={click}/>}
                <p>{text}</p>
                {actions}
            </div>
        </div>
}
Modal.propTypes={header:PropTypes.string, closeButton:PropTypes.bool, text:PropTypes.string, actions:PropTypes.element, click:PropTypes.func}
export default Modal;