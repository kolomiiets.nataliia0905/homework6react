import PropTypes from "prop-types";
function Button ({text, bgColor, click, className}) {
        return <button style={{background: bgColor}} className={className} onClick={click}>{text}</button>

}

Button.propTypes = {
    text: PropTypes.string.isRequired,
    bgColor: PropTypes.string,
    click: PropTypes.func.isRequired,
    className: PropTypes.string
}
Button.defaultProps = {
    className: ""
}
export default Button;
