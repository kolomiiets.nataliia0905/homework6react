import Card from "../card/Card";
import PropTypes from "prop-types";
import styles from "./List.module.css"
import {useDispatch, useSelector} from "react-redux";
import {changeModal, setFavorite} from "../../redux/actions/action";
import Button from "../Button/Button";
import {useMemo} from "react";
import Table from "../ table/ Table";
import useSettings from "../../hooks/useSettings";

function List({data, cardClick}) {
    const favorite = useSelector(store => store.favorite)
    const dispatch = useDispatch()
    const { settings, saveSettings } = useSettings();
    const buttonValues = cardClick ? {text: "Delete product (X)", type: "delete"} : {text: "Add to cart", type: "add"}
    const columns = useMemo(
        () => [
            {
                // first group - TV Show
                Header: "Products",
                // First group columns
                columns: [
                    {
                        Header: "Name",
                        accessor: "name",
                    },
                    {
                        Header: "Price",
                        accessor: "price",
                    },
                    {
                        Header: "Article",
                        accessor: "article",

                    },
                    {  Header: 'Image', Cell: tableProps  => (
                            <div><img className={styles.img} src={tableProps.row.original.image} />{tableProps.row.original.PlayerName}</div>
                        )},

                ],
            },

        ],
        []
    );

    // data state to store the TV Maze API data. Its initial value is an empty array


    return  <div className={"container"}>
        {settings.theme==='table'?<Table columns={columns} data={data} />:
        <ul className={styles.wrapper}>{data.map((product) => <Card key={product.id} product={product}
                                                                    cardClick={cardClick}
                                                                    isFavorite={favorite.includes(product.id)}
                                                                    handleFavorite={() => dispatch(setFavorite(product.id))}
                                                                    actionBtn={<Button text={buttonValues.text}
                                                                                       click={() => dispatch(changeModal(product.id, buttonValues.type))}/>}/>)}</ul>

        }   </div>

}

List.propTypes = {
    data: PropTypes.array,
    handleClick: PropTypes.func,
    handleFavorite: PropTypes.func,
    favorite: PropTypes.array
}
export default List;
