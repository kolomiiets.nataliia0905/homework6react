import Table from "../Table/TableCard";
import {useMemo} from "react";

export  const CardsLoyautWrapper=({data,cardClick})=>{
    const columns = useMemo(
        () => [
            {
                // first group - TV Show
                Header: "TV Show",
                // First group columns
                columns: [
                    {
                        Header: "Name",
                        accessor: "show.name",
                    },
                    {
                        Header: "Type",
                        accessor: "show.type",
                    },
                ],
            },
            {
                // Second group - Details
                Header: "Details",
                // Second group columns
                columns: [
                    {
                        Header: "Language",
                        accessor: "show.language",
                    },
                    {
                        Header: "Genre(s)",
                        accessor: "show.genres",
                    },
                    {
                        Header: "Runtime",
                        accessor: "show.runtime",
                    },
                    {
                        Header: "Status",
                        accessor: "show.status",
                    },
                ],
            },
        ],
        []
    );
    return (
        <div className="App">
            <Table columns={columns} data={data} />
        </div>
    );
}