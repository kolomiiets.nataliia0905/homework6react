import List from "../../list/List";
import {useSelector} from "react-redux";
import EmptyListChecker from "../Components/EmptyListChecker";
import useSettings from "../../../hooks/useSettings";

function Home() {

    const products = useSelector(store => {

        return store.products
    })
    return (
        <div>
            <EmptyListChecker checkArr={products} message={"Empty favorite list!"}>

                <List data={products}/>
            </EmptyListChecker>
        </div>
    );
}

export default Home;