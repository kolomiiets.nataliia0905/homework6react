
import List from "../../../list/List";
import {useSelector} from "react-redux";
import EmptyListChecker from "../../Components/EmptyListChecker";
import {CardForm} from "../../../CardForm/CardForm";
import styles from "./Basket.module.css"
function Basket() {
    const {basket,products} = useSelector(store=>store)
    return (
        <div className={styles.wrapper}>
            <EmptyListChecker checkArr={basket} message={"Empty basket list!"}>
                <List cardClick={true}
                      data={products.filter(product =>basket.includes(product.id))}/>
                <CardForm/>
            </EmptyListChecker>

        </div>
    );
}
export default Basket;