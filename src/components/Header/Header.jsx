import {ReactComponent as StarIcon} from "../../img/star-fill.svg"
import {ReactComponent as BasketIcon} from "../../img/basket.svg"
import PropTypes from "prop-types";
import styles from "./Header.module.css"
import {Link} from "react-router-dom";
import Button from "../Button/Button";

function Header({favLength, cardLength,saveSettings}) {
    return <header className={styles.header}>
        <nav>
            <ul className={styles.list}>
                <li className={styles.item}>
                    <Link to="/">Home</Link>
                </li>
                <li className={styles.item}>
                    <Link to="/favourite">Favourite<StarIcon className={styles.starIcon}/>{favLength}</Link>
                </li>
                <li className={styles.item}>
                    <Link to="/basket">Basket <BasketIcon className={styles.bascetIcon}/>{cardLength}</Link>
                </li>
            </ul>
        </nav>
        <Button text={"change products view"} click={saveSettings} />
    </header>
}

Header.propTypes = {favLength: PropTypes.number, cardLength: PropTypes.number}
export default Header;
