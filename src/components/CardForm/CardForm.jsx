import React from 'react';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import {PatternFormat} from "react-number-format";
import styles from "./CardForm.module.css"
import {useDispatch, useSelector} from "react-redux";
import {clearBasket} from "../../redux/actions/action";


export const CardForm = () => {
    const basket = useSelector(state => state.basket)
    const products = useSelector(state => state.products)
    const dispatch = useDispatch();
    const formik = useFormik({
            initialValues: {
                firstName: '',
                lastName: '',
                age: '',
                address: '',
                phone: '',
            },
            validationSchema: Yup.object({
                firstName: Yup.string()
                    .max(15, 'Must be 15 characters or less')
                    .required('Required'),
                lastName: Yup.string()
                    .max(20, 'Must be 20 characters or less')
                    .required('Required'),
                age: Yup.number().min(18, 'to order on the website, call your parents').required('Required'),

                address: Yup.string()
                    .required('Required'),
                phone: Yup.string().min(12, 'your number is short').required('Required')
                    .matches(/^\+38 \((?!0{3}\))\d{3}\) [1-9]\d{2}-\d{2}-\d{2}$/, "must be format:+38(000)000-00-00")

            }),
            onSubmit: values => {

                const filterArr = products.filter(product => basket.includes(product.id))
                dispatch(clearBasket())
                console.log("Контактная информация о клиенте")
                console.log(JSON.stringify(values, null, 2));
                console.log("Товары выбранные клиентом")
                console.log(JSON.stringify(filterArr))

            },
        }
    );
    return (
        <form className={styles.form} onSubmit={formik.handleSubmit}>
            <label htmlFor="firstName">First Name</label>
            <input
                id="firstName"
                name="firstName"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.firstName}
            />
            {formik.touched.firstName && formik.errors.firstName ? (
                <div className={styles.error}>{formik.errors.firstName}</div>
            ) : null}

            <label htmlFor="lastName">Last Name</label>
            <input
                id="lastName"
                name="lastName"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.lastName}
            />
            {formik.touched.lastName && formik.errors.lastName ? (
                <div className={styles.error}>{formik.errors.lastName}</div>
            ) : null}

            <label htmlFor="age">Age</label>
            <input
                id="age"
                name="age"
                type="age"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.age}
            />
            {formik.touched.age && formik.errors.age ? (
                <div className={styles.error}>{formik.errors.age}</div>
            ) : null}

            <label htmlFor="address">Your address</label>
            <input
                id="address"
                name="address"
                type="address"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.address}
            />
            {formik.touched.age && formik.errors.address ? (
                <div className={styles.error}>{formik.errors.address}</div>
            ) : null}


            <label htmlFor="phone">Your phone</label>
            <PatternFormat
                format="+38 (###) ###-##-##"
                id="phone"
                name="phone"
                type="phone"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.phone}
            />
            {formik.touched.age && formik.errors.phone ? (
                <div className={styles.error}>{formik.errors.phone}</div>
            ) : null}

            <button className={styles.button} type="submit">Submit</button>
        </form>)
};