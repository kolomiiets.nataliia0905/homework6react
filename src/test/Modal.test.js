
import {fireEvent, render} from "@testing-library/react";
import React from "react";
import Modal from "../components/Modal/Modal";
import Button from "../components/Button/Button";
import '@testing-library/jest-dom'
test('it should match the snapshot', () => {
    const { container } = render(<Modal text={"b hhjb hj j"}/>);
    expect(container).toMatchSnapshot();
});

describe("Modal", () => {
    it(" test modal prop text ", () => {
        const { getByText } = render(<Modal text="test" />);

        const buttonEl = getByText(/test/i);
        expect(buttonEl).toBeInTheDocument();
    });
    it("test button close", () => {
        const { getByText } = render(<Modal text="test" click={jest.fn()} closeButton={true}/>);

        const buttonEl = getByText(/X/i);
        expect(buttonEl).toBeInTheDocument();
    });
    it("test stop propogation", () => {
        const mockedOnStepClick = jest.fn();
        const { getByRole } = render(<Modal  />);
       const testElement = getByRole("modalContainer")
         testElement.onclick=mockedOnStepClick;
       fireEvent.click(testElement);


        expect(mockedOnStepClick).toHaveBeenCalledTimes(1);
    });
});