import { configureStore } from '@reduxjs/toolkit';
import {rootReducer} from "../redux/reducer/reducer";
import {setFavorite} from "../redux/actions/action";
describe('myReducer', () => {
    let store;

    beforeEach(() => {
        store = configureStore({
            reducer:rootReducer,
        });
    });
    it('should increment the counter', () => {
        store.dispatch(setFavorite(1));
        const mockDataExpect={  products: [],
            basket:  [],
            favorite: [1],
            modal:{type: null, id: null}}
        expect(store.getState()).toStrictEqual(mockDataExpect );
    });
});