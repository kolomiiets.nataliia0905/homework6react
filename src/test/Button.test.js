import Button from "../components/Button/Button";
import {render,fireEvent} from "@testing-library/react";
import React from "react";
import '@testing-library/jest-dom'
test('it should match the snapshot', () => {
    const { container } = render(<Button text="test"/>);
    expect(container).toMatchSnapshot();
});
describe("Button", () => {
    it(" test button prop text ", () => {
        const { getByText } = render(<Button text="test" click={jest.fn}/>);

        const buttonEl = getByText(/test/i);
        expect(buttonEl).toBeInTheDocument();
    });
    it("test button prop style", () => {
        const { getByRole } = render(<Button text="test" click={jest.fn} bgColor="red" />);
        const buttonEl = getByRole("button");
        expect(buttonEl).toHaveStyle("background:red");
    });
    it("test modal func", () => {
        const mockedOnStepClick = jest.fn();
        const { getByRole } = render(<Button text="test"  click={mockedOnStepClick} />);
        // eslint-disable-next-line testing-library/prefer-screen-queries
        fireEvent.click(getByRole("button"));
        expect(mockedOnStepClick).toHaveBeenCalledTimes(1);
    });
});